import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test_captain/Utils/PageNames.dart';
import 'package:test_captain/models/post.dart';

class PostWidget extends StatelessWidget {
  PostWidget({
    Key key,
    this.post,
  }) : super(key: key);
  final Post post;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () => Get.toNamed(NamePages.postDetails, arguments: post),
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Container(
          width: width * 0.9,
          height: 90,
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(post.title),
                  Row(
                    children: [
                      Text(
                        "is published: ",
                        style: TextStyle(
                          color: Colors.black26,
                        ),
                      ),
                      Icon(
                        post.published ? Icons.check : Icons.close,
                        color: post.published
                            ? Colors.green.withOpacity(0.6)
                            : Colors.red.withOpacity(0.6),
                        size: 12,
                      ),
                    ],
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(post.createdAt.toUtc().toString().substring(0, 10)),
                  Text("views: ${post.views}"),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
